# Analytic Tableaux (Elixir version for PBT fun!)

1. [Method of Analytic Tableaux](#method-of-analytic-tableaux)
2. [Elixir implementation](#elixir-implementation)
3. [Property-based testing](#property-based-testing)
4. [Instructions](#instructions)

## Method of analytic tableaux

An [analytic tableaux](https://en.wikipedia.org/wiki/Method_of_analytic_tableaux) is a tree structure that represents a logical formula, where each node is a proposition. A node can have either one child-leave, representing a conjunction:

[![Propositional tree (and)](priv/propositional_tree_and.jpg)](https://commons.wikimedia.org/wiki/File:Captura_algebra_para_la_edificaci%C3%B3n_1.JPG)

Or two children-leaves, representing disjunction. 

[![Propositional tree (or)](priv/propositional_tree_or.jpg)](https://commons.wikimedia.org/wiki/File:Captura_algebra_para_la_edificaci%C3%B3n_2.JPG)

## Elixir implementation

This project features a simple Elixir implementation that computes the validity of a propositional argument. The main module is called `tableaux`, that offers the main function `is_valid?/1`. This function accepts a string representing a proposition:

```
  iex -S mix
  iex > Tableaux.is_valid? "((p → q) ∧ ¬q) → ¬p"
  true
  iex > Tableaux.is_valid? "(p V q) → p"
  false
```

## Property-based testing

This project also features a set of properties aimed at increasing the confidence in the correctness of its implementation. These properties function as tests and are executed using [PropEr](https://proper-testing.github.io/userguide.html) (more specifically, [PropCheck](https://hexdocs.pm/propcheck/readme.html)). Being this an Elixir project, tests are run simply executing:

```
  mix test
```



## Instructions

A local Elixir installation is a pre-requisite to run this project.

Clone the repo and perform the following steps:

* `mix deps.get` in order to fetch project dependencies
* `mix test in` order to run project tests

Enjoy!
