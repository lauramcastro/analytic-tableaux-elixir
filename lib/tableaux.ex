defmodule Tableaux do
  @moduledoc """
  Documentation for `Tableaux`.
  """

  @type prop_tree ::
          {:leaf, String.t()}
          | {:logic_not, prop_tree}
          | {:logic_and, prop_tree, prop_tree}
          | {:logic_or, prop_tree, prop_tree}
          | {:logic_implies, prop_tree, prop_tree}

  @doc """
  Computes the validity of a propositional argument.

  ## Examples

      iex> Tableaux.is_valid? "(p V q) → p"
      false

  """
  @spec is_valid?(String.t()) :: boolean()
  def is_valid?(prop) do
    string_to_tree(prop) |> closed_tree()
  end

  @doc """
  Translates proposition from a string format to a tree format.
  ## Examples

      iex> Tableaux.string_to_tree "(p V q) → p"
      {:logic_implies, {:logic_or, {:leaf, 'p'}, {:leaf, 'q'}}, {:leaf, 'p'}}

  """
  @spec string_to_tree(String.t()) :: prop_tree()
  def string_to_tree(prop) do
    {:ok, tokens, _} = :lexer.string(String.to_charlist(prop))
    {:ok, tree} = :parser.parse(tokens)
    tree
  end

  @doc """
  Examines all branches of a given proposition (in tree format) to determine validity.

  ## Examples
      iex> Tableaux.closed_tree {:logic_implies, {:logic_or, {:leaf, 'p'}, {:leaf, 'q'}}, {:leaf, 'p'}}
      false
  """
  @spec closed_tree(prop_tree()) :: boolean()
  def closed_tree(tree) do
    do_closed_tree(tree) |> List.flatten() |> remove_opposites() |> Enum.empty?()
  end

  defp do_closed_tree({:logic_implies, first_subtree, second_subtree}) do
    do_closed_tree({:logic_or, {:logic_not, first_subtree}, second_subtree})
  end

  defp do_closed_tree({:logic_or, first_subtree, second_subtree}) do
    left_props = do_closed_tree(first_subtree)
    right_props = do_closed_tree(second_subtree)
    [left_props, right_props]
  end

  defp do_closed_tree({:logic_and, first_subtree, second_subtree}) do
    left_props = do_closed_tree(first_subtree)
    right_props = do_closed_tree(second_subtree)
    left_props ++ right_props
  end

  # negation of implication
  defp do_closed_tree({:logic_not, {:logic_implies, first_subtree, second_subtree}}) do
    do_closed_tree({:logic_and, first_subtree, {:logic_not, second_subtree}})
  end

  # negation of a conjunction is aggregation of negations
  defp do_closed_tree({:logic_not, {:logic_and, first_subtree, second_subtree}}) do
    do_closed_tree({:logic_or, {:logic_not, first_subtree}, {:logic_not, second_subtree}})
  end

  # negation of aggregation is conjunction of negations
  defp do_closed_tree({:logic_not, {:logic_or, first_subtree, second_subtree}}) do
    do_closed_tree({:logic_and, {:logic_not, first_subtree}, {:logic_not, second_subtree}})
  end

  # negation of negation gets cancelled
  defp do_closed_tree({:logic_not, {:logic_not, subtree}}) do
    do_closed_tree(subtree)
  end

  # negation of negation gets cancelled
  defp do_closed_tree({:logic_not, {:leaf, p}}) do
    [{:not, to_string(p)}]
  end

  defp do_closed_tree({:leaf, p}) do
    [to_string(p)]
  end

  defp remove_opposites(l), do: Enum.filter(l, fn x -> not has_opposite(x, l) end)

  defp has_opposite({:not, p}, l), do: Enum.member?(l, p)
  defp has_opposite(p, l), do: Enum.member?(l, {:not, p})
end
