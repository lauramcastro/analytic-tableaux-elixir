Definitions.
Rules.
%% a lowercase character representing a simple proposition
[a-z] : {token, {proposition, TokenLine, TokenChars}}.
%% open/close parentheses
\( : {token, {'(', TokenLine}}.
\) : {token, {')', TokenLine}}.
%% logic operators
\¬ : {token, {'¬', TokenLine}}.
\V : {token, {'V', TokenLine}}.
\∧ : {token, {'∧', TokenLine}}.
\→ : {token, {'→', TokenLine}}.
%% white space
[\s\n\r\t]+ : skip_token.
%%
Erlang code.
