Nonterminals predicate premise.
Terminals proposition '¬' 'V' '∧' '→' '(' ')'.
Rootsymbol predicate.

Left 100 'V' '∧' '→'.
Unary 200 '¬'. 

predicate -> premise '→' predicate : {logic_implies, '$1', '$3'}.
predicate -> premise '∧' predicate : {logic_and, '$1', '$3'}.
predicate -> premise 'V' predicate : {logic_or, '$1', '$3'}.
predicate -> premise : '$1'.

premise -> '(' predicate ')' : '$2'.
premise -> '¬' predicate : {logic_not, '$2'}.
premise -> proposition : {leaf, unwrap('$1')}.

Erlang code.

unwrap({_,_,V}) -> V.