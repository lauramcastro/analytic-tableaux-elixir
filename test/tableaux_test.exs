defmodule TableauxTest do
  use ExUnit.Case
  doctest Tableaux

  test "modus tollens" do
    assert Tableaux.is_valid?("((p → q) ∧ ¬q) → ¬p")
  end
end
